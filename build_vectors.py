# coding: utf-8
__author__ = 'dmaust'

"""
Benchmarks for the Avito fraud detection competition
"""
import csv
import re
import nltk.corpus
from collections import defaultdict
import scipy.sparse as sp
import numpy as np
import os
from sklearn.linear_model import SGDClassifier, RidgeClassifier, RidgeClassifierCV
from sklearn.feature_extraction.text import TfidfVectorizer, TfidfTransformer, CountVectorizer
from sklearn.feature_extraction import DictVectorizer

from nltk import SnowballStemmer
import random as rnd
import logging
from sklearn.externals import joblib
from sklearn.metrics import roc_auc_score
from sklearn.utils import shuffle
from collections import Counter
import math
import numpy
import json
import gzip
import multiprocessing

from sklearn.decomposition import TruncatedSVD
from sklearn.preprocessing import StandardScaler
from sklearn.kernel_approximation import RBFSampler, Nystroem

import itertools


dataFolder = "/home/dmaust/avitopredictor/data"

trainFileName = os.path.join(dataFolder,"avito_train.tsv.gz")
testFileName = os.path.join(dataFolder,"avito_test.tsv.gz")

stopwords= frozenset(word for word in nltk.corpus.stopwords.words("russian") if word!=u"не")
stemmer = SnowballStemmer('russian')
engChars = [ord(char) for char in u"cCyoOBaAKpPeE"]
rusChars = [ord(char) for char in u"сСуоОВаАКрРеЕ"]
eng_rusTranslateTable = dict(zip(engChars, rusChars))
rus_engTranslateTable = dict(zip(rusChars, engChars))
close_hour_threshold = 0.5 / 60.



def enable_logging():
    logging.basicConfig(format = u'[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level = logging.NOTSET, filename="vectorizer.log")

rusRegex = re.compile(ur"[а-я]")
engRegex = re.compile(ur"[a-z]")

def correctWord (w):
    """ Corrects word by replacing characters with written similarly depending on which language the word.
        Fraudsters use this technique to avoid detection by anti-fraud algorithms."""

    if len(rusRegex.findall(w))>len(engRegex.findall(w)):
        return w.translate(eng_rusTranslateTable)
    else:
        return w.translate(rus_engTranslateTable)

def getItems(fileName, itemsLimit=None):
    """ Reads data file. """

    with gzip.open(os.path.join(dataFolder, fileName), 'r') as items_fd:

        logging.info("Sampling done. Reading data...")
        itemReader=csv.DictReader(items_fd, delimiter='\t', quotechar = '"')
        itemNum = 0
        for i, item in enumerate(itemReader):
            item = {featureName:featureValue.decode('utf-8') for featureName,featureValue in item.iteritems()}
            itemNum += 1
            if itemNum % 1000 == 0:
                logging.info("Reading %d from %s...", itemNum, fileName)
            yield itemNum, item


wordChars = re.compile(u'[a-zа-я0-9]')

def getChars(text):
    cleanText = wordChars.sub(' ', text.lower())
    return Counter(cleanText)


nonWordChars = re.compile(u'[^a-zа-я0-9]')
engAlphaNum = re.compile("[0-9a-z]")

def getWords(text, stemmRequired = False, correctWordRequired = False):
    """ Splits the text into words, discards stop words and applies stemmer.
    Parameters
    ----------
    text : str - initial string
    stemmRequired : bool - flag whether stemming required
    correctWordRequired : bool - flag whether correction of words required
    """

    cleanText = nonWordChars.sub(' ', text.lower())
    if correctWordRequired:
        words = [correctWord(w) if not stemmRequired or engAlphaNum.search(w) else stemmer.stem(correctWord(w)) for w in cleanText.split() if len(w)>1 and w not in stopwords]
    else:
        words = [w if not stemmRequired or engAlphaNum.search(w) else stemmer.stem(w) for w in cleanText.split() if len(w)>1 and w not in stopwords]

    return words

def gaussian(x,y, sigma=0.25):
    return math.exp(-(x-y)**2 / sigma)

def gaussian_dict(value, text='value', linear=True):
    guassian_d =  { "%s_%d" % (text, i) :  gaussian(float(value), float(i)) for i in range(0, 100) if gaussian(value, float(i)) > 0.001 }
    if linear:
        guassian_d.update({"%s_linear" % (text,): value})
    return guassian_d


def sublinear(value):

    value = float(value)

    if value == 0.:
        return 0.
    else:
        return 1. + np.log(value)


def jsonFeatures(item):
    features = {}
    try :
        if item['attrs'] != '':
            for attr_key,attr_val in json.loads(item['attrs'].replace('/"', '')).items():
                features['attr_%s_%s' % (attr_key,attr_val)] = 1
    except:
        logging.warn("Failed to parse: %s" , item['attrs'])

    return features


def otherFeatures(item):
    features = {}

    text = item['title']+item['description']

    for char,count in getChars(text).items():
            features['char_%s' % (char,)] = float(count)


    features['category'] = item['category']
    features['subcategory'] = item['subcategory']

    try :
        price = sublinear(item['price'])
        features.update( gaussian_dict(price, 'price'))
    except Exception as inst:
        logging.warn("Invalid price", inst)

    try :
        phones = sublinear(item['phones_cnt'])
        features.update( gaussian_dict(phones, 'phones') )
    except Exception as inst:
        logging.warn("Invalid phones", inst)

    try :
        emails = sublinear(item['emails_cnt'])
        features.update( gaussian_dict(emails, 'emails') )
    except Exception as inst:
        logging.warn("Invalid emails", inst)

    try :
        urls = sublinear(item['urls_cnt'])
        features.update( gaussian_dict(urls, 'urls') )
    except Exception as inst:
        logging.warn("Invalid urls", inst)

    return features


def extractText(itemGen):
    textFeatures = (item["category"] + " "
                    + item["subcategory"] + " "
                    + item["title"]+" "
                    + item["description"] + " "
                    + item["attrs"] for index,item in itemGen)

    textFeaturesClean = (" ".join(getWords(text) + getWords(text, True, True)) for text in textFeatures)
    return textFeaturesClean

def extractNumeric(itemGen):
    return (otherFeatures(item) for index,item in itemGen)

def extractJson(itemGen):
    return (jsonFeatures(item) for index,item in itemGen)

def trainTextData(trainFileName, testFileName, itemsLimit=None, ngram=(1,1)):
    itemGen = getItems(trainFileName)
    if itemsLimit:
        itemGen = itertools.islice(itemGen, 0, itemsLimit)

    textFeatures = extractText(itemGen)
    vectorizer = CountVectorizer(min_df=5, ngram_range=ngram)
    trainFeature = vectorizer.fit_transform(textFeatures)

    itemGen = getItems(testFileName)
    textFeatures = extractText(itemGen)
    testFeature = vectorizer.transform(textFeatures)
    return trainFeature, testFeature, vectorizer.get_feature_names()


def trainNumericData(trainFileName, testFileName, itemsLimit=None):
    itemGen = getItems(trainFileName)

    if itemsLimit:
        itemGen = itertools.islice(itemGen, 0, itemsLimit)

    dictFeatures = extractNumeric(itemGen)
    vectorizer = DictVectorizer()
    trainFeature = vectorizer.fit_transform(dictFeatures)

    itemGen = getItems(testFileName)
    dictFeatures = extractNumeric(itemGen)

    testFeature = vectorizer.transform(dictFeatures)

    return trainFeature, testFeature, vectorizer.get_feature_names()

def trainJsonData(trainFileName, testFileName, itemsLimit=None):
    itemGen = getItems(trainFileName)

    if itemsLimit:
        itemGen = itertools.islice(itemGen, 0, itemsLimit)

    dictFeatures = extractJson(itemGen)
    vectorizer = DictVectorizer()
    trainFeature = vectorizer.fit_transform(dictFeatures)

    itemGen = getItems(testFileName)
    dictFeatures = extractJson(itemGen)

    testFeature = vectorizer.transform(dictFeatures)

    transformer = TfidfTransformer()
    trainFeature = transformer.fit_transform(trainFeature)
    testFeature = transformer.transform(testFeature)

    return trainFeature, testFeature, vectorizer.get_feature_names()


def getTargets(trainFileName, itemsLimit=None):
    itemGen = getItems(trainFileName)
    if itemsLimit:
        itemGen = itertools.islice(itemGen, 0, itemsLimit)
    targets = numpy.array([int(item['is_blocked']) for index, item in itemGen])
    return targets

def getCloseHours(trainFileName, itemsLimit=None):
    itemGen = getItems(trainFileName)
    if itemsLimit:
        itemGen = itertools.islice(itemGen, 0, itemsLimit)
    close_hours = numpy.array([float(item['close_hours']) for index, item in itemGen])
    return close_hours

def parseIsProved(value):
    if value == '1':
        return 1
    else:
        return 0

def getIsProved(trainFileName, itemsLimit=None):
    itemGen = getItems(trainFileName)
    if itemsLimit:
        itemGen = itertools.islice(itemGen, 0, itemsLimit)
    proved = numpy.array([parseIsProved(item['is_proved']) for index, item in itemGen])
    return proved


def getItemIds(fileName, itemsLimit=None):
    itemGen = getItems(fileName)
    if itemsLimit:
        itemGen = itertools.islice(itemGen, 0, itemsLimit)
    targets = numpy.array([int(item["itemid"]) for index, item in itemGen])
    return targets

def buildTfFeatures():
    trainTextFeature, testTextFeature, featureNames = trainTextData(trainFileName, testFileName)

    joblib.dump((trainTextFeature, testTextFeature), os.path.join(dataFolder,"tf_feature.pkl"))
    joblib.dump((featureNames,), os.path.join(dataFolder,"text_feature_names.pkl"))

def buildNgramFeatures():
    trainTextFeature, testTextFeature, featureNames = trainTextData(trainFileName, testFileName, ngram=(2,2))

    joblib.dump((trainTextFeature, testTextFeature), os.path.join(dataFolder,"ng_feature.pkl"))
    joblib.dump((featureNames,), os.path.join(dataFolder,"ng_feature_names.pkl"))

def buildNgIdfFeatures():

    trainTextFeature, testTextFeature = loadNgFeatures()
    transformer = TfidfTransformer(sublinear_tf=True)

    trainTextFeature = transformer.fit_transform(trainTextFeature)
    testTextFeature = transformer.transform(testTextFeature)

    joblib.dump((trainTextFeature, testTextFeature), os.path.join(dataFolder,"ngidf_feature.pkl"))


def buildTfIdfFeatures():

    trainTextFeature, testTextFeature = loadTfFeatures()
    transformer = TfidfTransformer(sublinear_tf=True)

    trainTextFeature = transformer.fit_transform(trainTextFeature)
    testTextFeature = transformer.transform(testTextFeature)

    joblib.dump((trainTextFeature, testTextFeature), os.path.join(dataFolder,"tfidf_feature.pkl"))

def buildNumericFeatures():

    trainTextFeature, testTextFeature, featureNames = trainNumericData(trainFileName, testFileName)

    joblib.dump((trainTextFeature, testTextFeature), os.path.join(dataFolder,"numeric_feature.pkl"))
    joblib.dump((featureNames,), os.path.join(dataFolder,"numeric_feature_names.pkl"))

def buildJsonFeatures():

    trainTextFeature, testTextFeature, featureNames = trainJsonData(trainFileName, testFileName)

    joblib.dump((trainTextFeature, testTextFeature), os.path.join(dataFolder,"json_feature.pkl"))
    joblib.dump((featureNames,), os.path.join(dataFolder,"json_feature_names.pkl"))


def buildLabels():

    targets = getTargets(trainFileName)
    testItemIds = getItemIds(testFileName)
    close_hours = getCloseHours(trainFileName)
    is_proved = getIsProved(trainFileName)

    joblib.dump((targets, testItemIds, close_hours, is_proved ), os.path.join(dataFolder,"labels.pkl"))

def buildPca():

    trainTextFeatures, testTextFeatures = loadTextFeatures()

    pca = TruncatedSVD(n_components=200, algorithm='arpack')
    trainTextFeatures = pca.fit_transform(trainTextFeatures)
    testTextFeatures = pca.transform(testTextFeatures)

    joblib.dump((trainTextFeatures, testTextFeatures), os.path.join(dataFolder,"pca_feature.pkl"))

    return pca

def buildNumPca():

    trainNumFeatures, testNumFeatures = loadNumericFeatures()

    scaler = StandardScaler(with_mean=False)
    pca = TruncatedSVD(n_components=100, n_iterations=10)
    trainNumFeatures = pca.fit_transform(scaler.fit_transform(trainNumFeatures))
    testNumFeatures = pca.transform(scaler.transform(testNumFeatures))

    joblib.dump((trainNumFeatures, testNumFeatures), os.path.join(dataFolder,"pca_num_feature.pkl"))

    return pca

def loadTfFeatures():
    return joblib.load(os.path.join(dataFolder,"tf_feature.pkl"))

def loadNgFeatures():
    return joblib.load(os.path.join(dataFolder,"ng_feature.pkl"))


def loadTextFeatures():
    return joblib.load(os.path.join(dataFolder,"tfidf_feature.pkl"))

def loadNgIdfFeatures():
    return joblib.load(os.path.join(dataFolder,"ngidf_feature.pkl"))

def loadPcaFeatures():
    return joblib.load(os.path.join(dataFolder,"pca_feature.pkl"))

def loadNumPcaFeatures():
    return joblib.load(os.path.join(dataFolder,"pca_num_feature.pkl"))

def loadNumericFeatures():
    return joblib.load(os.path.join(dataFolder,"numeric_feature.pkl"))


def loadLabels():
    return joblib.load(os.path.join(dataFolder,"labels.pkl"))
